oolite (1.84-2) unstable; urgency=medium

  * Debhelper 10. Remove explicit target confusing dh.
  * Standards-Version: 4.0.1. Only suggest -doc.
    HTTPS copyright format URL. Handle DEB_BUILD_OPTIONS=nodoc.
  * Update espeak dependency to espeak-ng. Closes: #844639.
  * Replace handwritten preinst with dpkg-maintscript-helper invocation.
  * Allow -doc and -data to satisfy Multi-Arch: foreign dependencies.
  * Drop obsolete Break/Replace with 1.74.2-2.
  * Install now separate image files alongside html docs.

  [ Clint Adams <clint@debian.org> ]
  * Reproducible locale in docs. Closes: #847021.

 -- Nicolas Boulenguez <nicolas@debian.org>  Sun, 13 Aug 2017 14:28:04 +0200

oolite (1.84-1) unstable; urgency=medium

  * New upstream version. Patches:
   - Initialize NSDate before playing with threads. Closes: #833134.
     Thanks to Ivan Vučica and Richard Frith-Macdonald.
   - Simplify preprocessor variables data flow, fixing minor issues.
   - Generating manifest.plist even without Xcode.
   - Remove various GCC or lintian style warnings.
   - Merge two existing patches improving desktop file.
  * Enable all hardening flags.
  * Install docs as (good) .odt and (ugly) HTML, but no more as (bad) PDF.
  * Switch to uscan version: 4 and Standards-Version: 3.9.8.
  * GNUstep now set -g by default, remove related tricks.
  * No need to install README.source in binary packages.

 -- Nicolas Boulenguez <nicolas@debian.org>  Tue, 02 Aug 2016 04:11:48 +0200

oolite (1.82-2) unstable; urgency=medium

  * README: avoid nvidia drivers without nvidia card. Closes: #702092.
  [ Petter Reinholdtsen <pere@hungry.com> ]
  * Add StrategyGame category to desktop file. Closes: #825680.
  [ Gianfranco Costamagna <locutusofborg@debian.org> ]
  * Make a cast explicit for glib-2.23. Closes: #818815.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 11 Jul 2016 14:56:24 +0200

oolite (1.82-1) unstable; urgency=medium

  * New upstream release, splitting sound and images. License update.
  * Drop unneeded build dependency on gnustep-core-devel. Closes: #749453.
  * gnustep >= 2.6.6-1 does not need let_gnustep_handle_cflags.diff anymore.
  * watch: upstream migrated to github. Closes: #755994.
  * Repackage with Files-Excluded instead of cleaner script and git branch.
  * Reactivate libespeak support, see #591717.
  * Standards-Version: 3.9.6 (no changes).
  * debian/README: web page snippets that may be useful for users.
  * Provide a writable XDG_CONFIG_HOME to libreoffice when converting .odt
    to PDF, lowering the severity of #730893.
  * allow -Werror=format-security, default with gcc-5. Closes: #778039.
  * patches/avoid_date_cpp_macro.diff: for reproducible builds.
  * patches/avoid_infinite_recursion_in_verify_descriptions.diff.

 -- Nicolas Boulenguez <nicolas@debian.org>  Thu, 03 Sep 2015 01:35:02 +0200

oolite (1.77.1-3) unstable; urgency=medium

  * Avoid patches/gnustep_app_builder.diff. Closes: #732377.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 18 Dec 2013 19:35:21 +0100

oolite (1.77.1-2) unstable; urgency=low

  * The symlink in oolite described below replaces a directory in
    oolite-data<<1.76.1-3, Break+Replace accordingly. Closes: #729219.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 11 Nov 2013 12:51:22 +0100

oolite (1.77.1-1) unstable; urgency=low

  * New upstream snapshot. One patch refreshed.
  * rules, Build-Depends-Indep: generate some PDF from odt sources.
  * copyright, dfsg_cleaner: reduce differences between upstream and DFSG
    tarballs. Document the newly embedded licenses.
  * patches/generate_planet_data: generate 14Mo at build time.
  * Standards-Version: 3.9.5 (no changes).
  * oolite.preinst: symlink mentioned in 1.76.1-3 stanza may replace an
    existing directory, even if dpkg refuses to do so. Closes: #728790.

 -- Nicolas Boulenguez <nicolas@debian.org>  Wed, 06 Nov 2013 00:54:49 +0100

oolite (1.76.1-3) unstable; urgency=low

  * Updated patches/debian_version_of_libmozjs.
    New patches/add_keywords_to_desktop_file, gnustep_app_builder.diff.
  * control (Build-Depends): removed work-around for #678859.
  * README.Debian, source/options, DM-Upload-Allowed: removed.
  * README.source, Vcs-*: canonical URL for git repository.
  * *.install, .links: replace arch-indep data in usr/lib with a symlink.
  * doc-base support for OoliteReadMe.pdf.
  * Standards-Version: 3.9.4. Split build-[arch|indep] targets.

 -- Nicolas Boulenguez <nicolas@debian.org>  Mon, 12 Aug 2013 21:21:14 +0200

oolite (1.76.1-2) unstable; urgency=low

  * control (Build-Depend), rules, patches/debian_version_of_libmozjs.diff:
    use quite stable libmozjs185-dev instead of libmozjs-dev, that tracks
    firefox's latest API.
  * rules: clarified which settings may be modified in a Debian build.
    CPPFLAGS, CFLAGS, noopt, nostrip -> GNUSTEP.
    parallel=n -> Make.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Mon, 25 Jun 2012 19:55:39 +0200

oolite (1.76.1-1) unstable; urgency=low

  * New upstream release.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Tue, 29 May 2012 21:16:33 +0200

oolite (1.76dfsg-1) unstable; urgency=low

  * Fake upstream release to correct 1.76.orig.tgz content. It did contain
    the full upstream branch instead of the dfsg_clean branch.
    Adapted watch file with dversionmangle.
  * patches/debian_version_of_libmozjs: refreshed.
  * patches/timer_allocation_bug: backported from upstream VCS, should
    close #665839.
  * Debhelper 9, Standards-Version 3.9.3 (no changes).
  * rules: use all available processors if DEB_BUILD_OPTIONS does not
    specify parallel=n.
  * README.Debian: remove obsolete part.
  * README.source: updated, dpkg-source removes patches automatically.
  * copyright: stand-alone detailed paragraph, updated copyrights.
  * rules: updated copyrights

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sat, 12 May 2012 21:52:34 +0200

oolite (1.76-1) unstable; urgency=low

  * New upstream.
  * watch: scan upstream stable releases instead of dev snapshots.
  * control: use default gobjc instead of explicit 4.6.
  * rules: use dpkg-dev build flags.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 22 Dec 2011 00:22:39 +0100

oolite (1.75.3-4) unstable; urgency=low

  * control (Build-Depends): not g++ (was needed for libmozjs)
    dpkg (>= 1.16.1) to set build flags
    gobjc-4.6 to get an explicit FTBFS on next gnustep-dev upgrade
  * copyright: (cosmetic) versioned DEP-3 format.
  * patches/debian_version_of_libmozjs.path: work-around two backward
    incompatibilities introduced by libmozjs-dev (Closes: #643972)

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 06 Oct 2011 13:31:41 +0200

oolite (1.75.3-3) unstable; urgency=low

  * rules: compiles with gobjc-4.6, as current gnustep runtime.
  * patches: respect DEP-3
  * copyright: respects DEP-5

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Mon, 19 Sep 2011 18:42:30 +0200

oolite (1.75.3-2) unstable; urgency=low

  * patches: use debian mozjs v6 (Closes: #639075).
  * gbp.conf: handle new git-buildpackage option for dfsg tarballs.
  * rules: --as-needed linker option to remove dpkg-shlibdeps warning.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 25 Aug 2011 20:39:06 +0200

oolite (1.75.3-1) unstable; urgency=low

  * New upstream release.
  * gbp.conf, source/local-options: clarified git-buildpackage configuration.
  * compat, control: switched to debhelper 8.
  * patches/libmojs5_not_4.diff: upstream uses mozjs v4, debian v5.
  * control: made dh targets more generic.
  * Upload to unstable with libmozjs>=4 (Closes: #614277).

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sat, 16 Jul 2011 14:57:16 +0200

oolite (1.75.2-2) experimental; urgency=low

  * Recompiled with gobjc-4.5, compatible with the current gnustep runtime,
    allthough gobjc-4.6 is now the default compiler in unstable.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sun, 15 May 2011 23:17:45 +0200

oolite (1.75.2-1) experimental; urgency=low

  * New upstream beta release.
  * control: Standards-Version 3.9.2 (no changes)
  * patches: src/Core/OOCPUInfo.h to consult sys/param.h instead of an
    explicit list of architectures to configure endianness. Should help #614277.
  * Updated manpage from upstream debian/ subdir.
  * Removed libespeak support because of #591717.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Tue, 10 May 2011 01:04:17 +0200

oolite (1.75.1-20110315-1) experimental; urgency=low

  * New upstream release. Huge diff because of CRLF elimination.
  * Use system libmozjs-dev instead of embedded one which caused some FTBS.
    (control:Build-Depends, copyright, mozjs.LICENSE, mozjs.LEGAL, rules)
  * Break circular dependency between binary packages (Closes: #616001)
    (control:Depends, rules).
  * Now builds with gcc-4.5 (Closes: #565101).
  * Allowed date component in upstream version number (watch).

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Fri, 25 Mar 2011 21:59:04 +0100

oolite (1.75-1) unstable; urgency=low

  * New upstream release.
  * DFSG compliant tarball and twice smaller. It embeds the source of
    mozilla javascript library (aka spidermonkey). Oolite needs the version
    shipped with iceweasel 4.0, not yet in libmozjs-dev debian package.
    Upstream downloads it at compile-time, wich is worst.
  * Renamed README->README.Debian so that it is embedded.
  * README.source documenting use of git-buildpackage for debian packaging.
  * control: four binary packages: oolite, -doc, -data and -data-sounds.
    (Uploaders, Maintainers) Removed Eddy and Debian Games Team.
    (Breaks, Replaces) to handle the split of -data (Closes: #614185).
    (Vcs-*) point to alioth git repository instead of old svn.
    (Build-Depends) libnspr4-dev, zip, python for embedded mozjs.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Sat, 26 Feb 2011 01:18:27 +0100

oolite (1.74.2-1) unstable; urgency=low

  * New upstream release (closes: #506809).
  * Merged oolite and oolite-data old packaging to share same source.
  * control(Build-Depends): oolite needs its own version of libmozjs.
  * copyright: upstream is now released under the GPL.

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 05 Aug 2010 00:49:00 +0200

oolite (1.65-7) unstable; urgency=low

  [ Eddy Petrișor ]
  * properly install the lintian override
  * override the emty-dir lintian warning since we actually need and
    empty dir

  [ Nicolas Boulenguez ]
  * Switch to dpkg-source 3.0 (quilt) format
  * rules: used debhelper overrides
  * moved non patch files from debian/patches to debian
  * renamed debhelper files (foo->oolite.foo) to comply to recent
    guidelines and prepare sharing source with oolite-data
  * removed dirs debhelper files, no more needed
  * control: added Homepage, Standards-Version 3.9.0, added myself to uploaders
  * copyright: added explicit copyright dates, broken very long lines.
  * doc-base: registered reference sheet
  * Closes: 578310 by recompilating with current headers

 -- Nicolas Boulenguez <nicolas.boulenguez@free.fr>  Thu, 08 Jul 2010 16:16:52 +0200

oolite (1.65-6) unstable; urgency=low

  [ Cyril Brulebois ]
  * Added Vcs-Svn and Vcs-Browser fields in the control file.

  [ Eddy Petrișor ]
  * changes (closes: #449101, #447465) according to
    http://lists.alioth.debian.org/pipermail/pkg-games-devel/2007-July/004000.html
    - use gs_make instead of sourcing
      /usr/lib/GNUstep/System/Makefiles/GNUstep.sh and running $(MAKE)
    - bumped versioned build depends on libgnustep-base-dev to (>= 1.14.0)
    - added a Build-Depends on quilt since we needed a patch to obtain this
  * install the .desktop file; patch thanks to Mario Bonino from Ubuntu

  [ Gerfried Fuchs ]
  * Removed leading The from the synopsis.

  [ Eddy Petrișor ]
  * bump debhelper compatibiliy to 5; updated builddep on debhelper
    accordingly
  * removed no longer needed XS-Autobuild

  [ Jon Dowland ]
  * add Homepage: control field to source stanza

 -- Jon Dowland <jon@alcopop.org>  Mon, 03 Dec 2007 17:46:28 +0000

oolite (1.65-5) unstable; urgency=low

  * remove debian/svn-deblayout from the source since it holds 0 value for the
    actual source package
  * the game was relicensed by upstream with the dual license model (GNU GPL
    and CC-NC-SA) - this was acknowledged to be done also retroactively for
    the current stable release of the game. Here is a quote from upstream's
    announce on its forum:
    The current official release, 1.65, may be considered to be dual-licensed;
    that is, it may be distributed under its current license (the Creative
    Commons Attribution-NonCommercial-ShareAlike 2.0 license, herafter known
    as CC-by-nc-sa-2) or the GPL.
  * change section from non-free/games to games as a consequence of the former
  * remove the justification from copyright for autobuilding this package since
    will be autobuild in main from now on
  * update TODO to remember to update the copyright info when the new release
    will be done

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Wed,  7 Mar 2007 01:06:03 +0200

oolite (1.65-4) unstable; urgency=low

  * changed XS-X-VCS-SVN into XS-VCS-SVN after the discussion on debian devel
  * also ship the TXT.rtf file which is actually a replacement for PLAYING.txt
  * added two more files with pointers to online documentation about the game:
    advanced_instructions.readme and first_flight.readme
  * remove "Application" from the categories list in the desktop file since is
    not a valid category
  * added the "XS-Autobuild: yes" line in the control file and added reasons
    in the copyright file (after Andi white-listed the package)
    Done as requested in:
    http://lists.debian.org/debian-devel-announce/2006/11/msg00012.html
  * install the Oolite reference sheet which was hidden in
    installers/win32/OoliteRS.pdf
  * fix the mail format in README.Debian
  * no longer install README_LINUX.TXT since is useless to Debian users
  * added a PLAYING.TXT with pointers about the contents of the installed
    documentation

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Fri, 22 Dec 2006 17:26:39 +0200

oolite (1.65-3) unstable; urgency=low

  * build depend on non-versioned libgnustep-base-dev instead of the
    versioned variant because of the GNUstep transition (Closes: #389855)
  * added the xs-x-vcs-svn header to the control file
  * changed the locale language code for Romanian in the desktop file from ro
    to ro_RO since the short form is the last to be tested (marginal speed
    optimisation)
  * updated package TODO file (nothing to do :-)

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Wed, 27 Sep 2006 21:11:23 +0300

oolite (1.65-2) unstable; urgency=low

  * added versioned dependency on oolite-data, since it crashes with older
    data sets

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Wed, 26 Jul 2006 11:50:59 +0300

oolite (1.65-1) unstable; urgency=low

  * new upstream release (closes: #378875)
  * changed the watch file as upstream has changed the naming convention
  * removed dependency on sdl-gfx and gnustep-gui since upstream managed to
    remove them.
  * it is no longer needed to have Info-Oolite.plist in the debian part of the
    package, it is now in the source tarball
  * go all the way using debhelper and install the lintian override using it
  * some files changed their location in the upstream source, synced the package

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Wed, 26 Jul 2006 10:47:52 +0300

oolite (1.62-5-3) unstable; urgency=low

  * Make the Homepage: "field" match the regexp from Debian Develper's
    Reference, best packaging practices, chapter "6.2.4 Upstream home page"

 -- Eddy Petrişor <eddy.petrisor@gmail.com>  Mon, 29 May 2006 02:07:10 +0300

oolite (1.62-5-2) unstable; urgency=low

  * Added a .desktop file
  * "The fix was already there" fix: typo in debian/control is now visible;
    wrong build was uploaded
  * removed commented call to dh_makeshlibs in debian/rules
  * bump standards version (nothing to do) - 3.7.2
  * fix watch file

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Sun, 16 Apr 2006 19:41:25 +0300

oolite (1.62-5-1) unstable; urgency=low

  * Initial release Closes: #361704

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Sun,  9 Apr 2006 21:56:57 +0300

